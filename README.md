# AWS Pyspark & Pipenv
Container prebuilt with the requirements for running Spark ETL jobs in AWS Glue and pipenv as a python dependency manager.

Includes:
Spark 2.4.3 and Python 3.6

```
docker build -t pyspark-pipenv .
docker run pyspark-pipenv -c 'pipenv run python ./my-etl.py'
```
