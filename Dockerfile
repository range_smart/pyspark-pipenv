# Docker file based off https://github.com/awslabs/aws-data-wrangler/blob/master/testing/Dockerfile
FROM openjdk:8-jre-slim

ARG SPARK_VERSION=2.4.3
ARG PYTHON_VERSION=3.6.8
ARG JAVA_SDK_VERSION=1.7.4
ARG HADOOP_VERION=2.7.3

RUN apt-get update -y -qq && \
  apt-get install -y -qq jq make build-essential libssl-dev zlib1g-dev libbz2-dev \
    libreadline-dev libsqlite3-dev wget curl llvm libncurses5-dev libncursesw5-dev \
    xz-utils tk-dev libffi-dev liblzma-dev python-openssl git gcc curl postgresql libpq-dev && \
  rm -rf /var/lib/apt/lists/*

RUN useradd -m python_user
ENV WORKDIR=/usr/local/src
RUN mkdir -p $WORKDIR && chown -R python_user:python_user $WORKDIR
USER python_user
ENV HOME  /home/python_user
ENV PYENV_ROOT $HOME/.pyenv
ENV PATH $PYENV_ROOT/shims:$PYENV_ROOT/bin:$PATH

RUN curl https://pyenv.run | bash

RUN echo $'export PATH="$HOME/.pyenv/bin:$PATH"  \n\
eval "$(pyenv init -)"                           \n\
eval "$(pyenv virtualenv-init -)"                \n'\
>> $HOME/.bashrc

RUN pyenv install ${PYTHON_VERSION} && \
  pyenv global ${PYTHON_VERSION} && \
  pyenv rehash

ENV PYSPARK_PYTHON=python
ENV PIP=$PYENV_ROOT/shims/pip
RUN $PIP install --upgrade pip && \
  $PIP install --no-cache-dir pyspark==${SPARK_VERSION} pipenv

RUN export SPARK_HOME=$(python -c "import sys; print([x for x in sys.path if x.endswith('site-packages')][0])")/pyspark && \
    curl --url "https://repo1.maven.org/maven2/com/amazonaws/aws-java-sdk/${JAVA_SDK_VERSION}/aws-java-sdk-${JAVA_SDK_VERSION}.jar" --output ${SPARK_HOME}/jars/aws-java-sdk-${JAVA_SDK_VERSION}.jar && \
    curl --url "https://repo1.maven.org/maven2/org/apache/hadoop/hadoop-aws/${HADOOP_VERSION}/hadoop-aws-${HADOOP_VERSION}.jar" --output ${SPARK_HOME}/jars/hadoop-aws-${HADOOP_VERSION}.jar && \
    mkdir -p ${SPARK_HOME}/conf && \
    echo spark.hadoop.fs.s3.impl=org.apache.hadoop.fs.s3a.S3AFileSystem >> ${SPARK_HOME}/conf/spark-defaults.conf

WORKDIR $WORKDIR

COPY --chown=python_user:python_user Pipfile* $WORKDIR/
RUN pipenv install --dev --ignore-pipfile

COPY --chown=python_user:python_user . $WORKDIR/
